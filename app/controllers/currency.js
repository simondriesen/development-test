// require currency model
const Currency = require('../models/currency');
const request = require('request');

// require API key
const api = require('../../config/api');

// Get last months exchange rates (multiple API calls but synchronous, complex API calls are behind paywall)
const getExchangeRatesFromAPI = () => {
    let today = new Date();
    let month = (today.getMonth() > 0) ? ("0" + (today.getMonth())).slice(-2) : '01'; // get past month
    let year = (today.getMonth() > 0) ? today.getFullYear() : today.getFullYear() - 1;
    let days = new Date(year, month, 0).getDate(); // count days in past month
    return new Promise((resolve, reject) => {
        let exchangeRates = [];
        for (let i = 1; i <= days; i++) { // API call for every day in past month
            let date = year + '-' + month + '-' + ("0" + (i)).slice(-2);
            let options = {
                url: 'http://data.fixer.io/api/' + date + '?access_key=' + api.key,
                json: true
            };
            request(options, (err, res, data) => {
                if (err) {
                    reject(err);
                }
                exchangeRates.push(data);
                // make sure all API calls are finished before resolving
                if (exchangeRates.length == days) {
                    resolve(exchangeRates);
                }
            });
        }
    });
}

// Get currencies from exchange rates and create them if they don't exist yet
const createCurrencies = (data) => {
    return new Promise((resolve, reject) => {
        for(let i in data) {
            exchangeRate = data[i];
            let keys = [];
            Object.keys(exchangeRate.rates).forEach(key => {
                keys.push(key);
            });
            for(let i in keys) {
                let conditions = {
                    code: keys[i]
                }
                let update = {
                    code: keys[i]
                };
                let options = {
                    upsert: true // create new if doens't exist yet
                }
                Currency.findOneAndUpdate(conditions, update, options, (err, docs) => {
                    if (err) {
                        reject(err);
                    }
                    if (i == data.length) {
                        resolve(docs);
                    }
                });
            }  
        }
    });
}

// Update currencies exchange rates with latest data
const updateExchangeRates = (data) => {
    return new Promise((resolve, reject) => {
        let counter = 0;
        for(let i in data) {
            Object.keys(data[i].rates).forEach(key => {
                let conditions = {
                    code: key,
                    'exchangeRates.date': {$ne: data[i].date}
                }
                let update = {
                    $push: {
                        exchangeRates: {
                            date: data[i].date,
                            base: data[i].base,
                            rate: data[i].rates[key]
                        }
                    }
                };
                Currency.findOneAndUpdate(conditions, update, (err, docs) => {
                    if (err) {
                        reject(err);
                    }
                    counter++;
                    if (counter == data.length) {
                        resolve(docs);
                    }
                });
            });
        }
    });
}

// Add extra info to currency from country API
const updateCurrency = (currency, name, symbol) => {
    return new Promise((resolve, reject) => {
        let conditions = {
            _id: currency._id
        }
        let update = {
            name: name,
            symbol: symbol,
        };
        
        Currency.findOneAndUpdate(conditions, update, (err, docs) => {
            if (err) {
                reject(err);
            } else {
                resolve();
            }
        });
    });
}

const getCurrency = (code) => {
    return new Promise((resolve, reject) => {
        let conditions = {
            code: code
        }
        Currency.findOne(conditions, (err, docs) => {
            if (err || !docs) {
                reject(err);
            } else {
                resolve(docs);
            }
        });
    });
}

const getAvrageExchangeRates = (currency, startDate, endDate) => {
    return new Promise((resolve, reject) => {
        let rates = [];
        for(let i in currency.exchangeRates) {
            let date = new Date(currency.exchangeRates[i].date);
            if (date > startDate && date < endDate ){
                rates.push(currency.exchangeRates[i].rate)
            }
        }
        let amount = rates.length;
        let sum = rates.reduce((a, b) => a + b, 0);
        let average = sum/amount;
        resolve((Math.round(average * 100)/100).toFixed(2));
    });
}

module.exports = {
    get: getCurrency,
    create: createCurrencies,
    update: updateCurrency,
    getFromAPI: getExchangeRatesFromAPI,
    getAvrageExchangeRates: getAvrageExchangeRates,
    updateExchangeRates: updateExchangeRates
};
