// require country model
const Country = require('../models/country');
const request = require('request');

const currencyController = require('./currency');

// Get countries from API
const getCountriesFromAPI = () => {
    let options = {
		url: 'https://restcountries.eu/rest/v2/all?fields=name;callingCodes;capital;population;currencies;flag',
		json: true
	};
    return new Promise((resolve, reject) => {
        request(options, (err, res, data) => {
            if (err) {
				reject(err);
			} else {
				resolve(data);
			}
        });
    });
}

// Create countries
const createCountries = (countries) => {
    return new Promise((resolve, reject) => {
        for(let i in countries) {
            let curr = countries[i].currencies[0];
            currencyController.get(curr.code).then((currency) => {
                currencyController.update(currency, curr.name, curr.symbol).then(() => {
                    let conditions = {
                        name: countries[i].name
                    }
                    let update = {
                        name: countries[i].name,
                        calling_code: countries[i].callingCodes[0],
                        capital: countries[i].capital,
                        population: countries[i].population,
                        currency: currency._id,
                        flag: countries[i].flag
                    };
                    let options = {
                        upsert: true // create new if doesn't exist yet
                    }
                    Country.findOneAndUpdate(conditions, update, options, (err, docs) => {
                        if (err) {
                            reject(err);
                        } else {
                            resolve(docs);
                        }
                    });
                }).catch((err) => {
                    reject(err);
                });
            }).catch(() => {
                // no match found
            });
        }
    });
}

// Get countries
const getCountries = () => {
    return new Promise((resolve, reject) => {
        Country.find((err, docs) => {
            if (err) {
                reject(err);
            } else {
                resolve(docs);
            }
        }).populate('currency');
    });
}

module.exports = {
    get: getCountries,
    create: createCountries,
    getFromAPI: getCountriesFromAPI
};
