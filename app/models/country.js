const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// schema for countries
const countrySchema = new Schema({
    name: String,
    calling_code: String,
    capital: String,
    population: Number,
    currency: {
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'Currency'
    },
    flag: String
});

// creates and exposes the currency model
module.exports = mongoose.model('Country', countrySchema);
