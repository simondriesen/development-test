const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// schema for currencies
const currencySchema = new Schema({
    code: String,
    name: String,
    symbol: String,
    exchangeRates: [
        {
            date: Date,
            base: String,
            rate: Number
        }
    ]
});

// creates and exposes the currency model
module.exports = mongoose.model('Currency', currencySchema);
