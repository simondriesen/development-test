const mongoose      = require('mongoose');
const readline      = require('readline');
const chalk         = require('chalk');
const fs            = require('fs');
const path          = require('path');
const PDFDocument   = require('pdfkit');

// require controllers and db config
const controllers = require('./app/controllers/index');
const db = require('./config/db');

// connection with mongoDB database.
mongoose.connect(db.url, { useNewUrlParser: true });
mongoose.set('useFindAndModify', false);

// retrieving user input though CLI
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

const optionsString = chalk.bold('What would you like to do? \n') + 
    chalk.bold('[1]') + ' Retrieve latest exchange rates from the API and store in our db. \n' + 
    chalk.bold('[2]') + ' Generate pdf file with last months average exchange rates. \n' + 
    chalk.bold('[3]') + ' Exit \n' + 
    chalk.bold('Select an option: ');

const options = () => {
    rl.question(optionsString, (answer) => {
        switch(answer) {
            case '1':
                updateDB();
                break;
            case '2':
                writePDF();
                break;
            case '3':
                process.exit();
                break;
            default:
                console.log(chalk.red('Please type one of the options [1,2,3]'));
                options();
        }
    });
}

const updateDB = () => {
    controllers.currency.getFromAPI().then((currencies) => {
        console.log(chalk.green('Exchange rates retrieved from API'));
        controllers.currency.create(currencies).then(() => {
            console.log(chalk.green('Currencies successfully updated in db.'));
            controllers.currency.updateExchangeRates(currencies).then(() => {
                console.log(chalk.green('Exchange rates successfully updated in db.'));
                controllers.country.getFromAPI().then((countries) => {
                    console.log(chalk.green('Countries retrieved from API'));
                    controllers.country.create(countries).then(() => {
                        console.log(chalk.green('Countries successfully updated in db.\n'));
                        options();
                    }).catch((err) => {
                        console.log(chalk.red(err));
                        options();
                    });
                }).catch((err) => {
                    console.log(chalk.red(err));
                    options();
                });
            }).catch((err) => {
                console.log(chalk.red(err));
                options();
            });
        }).catch((err) => {
            console.log(chalk.red(err));
            options();
        });
    }).catch((err) => {
        console.log(chalk.red(err));
        options();
    });
}

const writePDF = () => {
    doc = new PDFDocument;
    doc.pipe(fs.createWriteStream('./documents/report.pdf'));

    // Title
    let today = new Date();
    let month = (today.getMonth() > 0) ? ("0" + (today.getMonth())).slice(-2) : '01';
    let year = (today.getMonth() > 0) ? today.getFullYear() : today.getFullYear() - 1;
    let days = new Date(year, month, 0).getDate();
    let date = new Date(year + '/' + month + '/01');
    month = date.toLocaleString("en-us", { month: "long" });
    doc.fontSize(20).fillColor('#2196f3').text('Average exchange rates (euro)', 50, 50);
    doc.fontSize(10).fillColor('black').text(month + ' ' + year, 0, 58, {
        width: doc.page.width - 50,
        align: 'right'
    });
    doc.moveTo(50, 75).lineWidth(.5).lineTo((doc.page.width - 50), 75).strokeOpacity(.7).stroke();

    // Loop over countries
    controllers.country.get().then((countries) => {
        let y = 85;
        let counter = 0;
        doc.fontSize(7).font('Helvetica-Bold').text('country', 50, y).text('population', 150, y).text('capital', 200, y).text('calling code', 275, y)
        .text('currency', 325, y).text('symbol', 400, y).text('code', 450, y).text('exchange rate', 490, y);
        for(let i in countries) {
            let startDate = new Date(year + '/' + month + '/01');
            let endDate = new Date(year + '/' + month + '/' + days);
            controllers.currency.getAvrageExchangeRates(countries[i].currency, startDate, endDate).then((average) => {
                counter++;
                if(y >= 700) {
                    doc.addPage();
                    y = 50;
                } else {
                    y = y + 12.5;
                }
                let country = countries[i].name;
                if(country.length > 22) country = country.substring(0,22) + '...';
                let capital = countries[i].capital;
                if(capital.length > 17) capital = capital.substring(0,17) + '...';
                let currency = countries[i].currency.name;
                if(currency.length > 15) currency = currency.substring(0,15) + '...';
                doc.font('Helvetica').text(country, 50, y).text(countries[i].population, 150, y).text(capital, 200, y).text(countries[i].calling_code, 275, y)
                .text(currency, 325, y).text(countries[i].currency.symbol, 400, y).text(countries[i].currency.code, 450, y).fillColor('#2196f3').text(average, 490, y).fillColor('black');
                if(counter == countries.length) {
                    doc.end();
                    let fullpath = path.resolve('./info/report.pdf');
                    console.log(chalk.green('PDF generated: "' + fullpath + '"\n'));
                    options();
                }
            });
        }
    }).catch((err) => {
        console.log(chalk.red(err));
        options();
    });
};

options();
